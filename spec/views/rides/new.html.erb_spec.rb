require 'rails_helper'

RSpec.describe "rides/new", type: :view do
  before(:each) do
    assign(:ride, Ride.new(
      :origin => "MyString",
      :destination => "MyString"
    ))
  end

  it "renders new ride form" do
    render

    assert_select "form[action=?][method=?]", rides_path, "post" do

      assert_select "input#ride_origin[name=?]", "ride[origin]"

      assert_select "input#ride_destination[name=?]", "ride[destination]"
    end
  end
end
