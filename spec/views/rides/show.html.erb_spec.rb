require 'rails_helper'

RSpec.describe "rides/show", type: :view do
  before(:each) do
    @ride = assign(:ride, Ride.create!(
      :origin => "Origin",
      :destination => "Destination"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Origin/)
    expect(rendered).to match(/Destination/)
  end
end
