require 'rails_helper'

RSpec.describe "rides/edit", type: :view do
  before(:each) do
    @ride = assign(:ride, Ride.create!(
      :origin => "MyString",
      :destination => "MyString"
    ))
  end

  it "renders the edit ride form" do
    render

    assert_select "form[action=?][method=?]", ride_path(@ride), "post" do

      assert_select "input#ride_origin[name=?]", "ride[origin]"

      assert_select "input#ride_destination[name=?]", "ride[destination]"
    end
  end
end
