require 'rails_helper'

RSpec.describe "rides/index", type: :view do
  before(:each) do
    assign(:rides, [
      Ride.create!(
        :origin => "Origin",
        :destination => "Destination"
      ),
      Ride.create!(
        :origin => "Origin",
        :destination => "Destination"
      )
    ])
  end

  it "renders a list of rides" do
    render
    assert_select "tr>td", :text => "Origin".to_s, :count => 2
    assert_select "tr>td", :text => "Destination".to_s, :count => 2
  end
end
