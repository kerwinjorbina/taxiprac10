json.array!(@rides) do |ride|
  json.extract! ride, :id, :origin, :destination
  json.url ride_url(ride, format: :json)
end
