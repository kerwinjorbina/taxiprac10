class Ride < ActiveRecord::Base
    validates :origin, :destination, presence: true, length: { minimum: 1 }
end
